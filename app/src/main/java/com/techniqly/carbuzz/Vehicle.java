package com.techniqly.carbuzz;

/**
 * Created by Farooq on 8/22/2016.
 */
public final class Vehicle {
    public static final Vehicle[] Vehicles = {
            new Vehicle(1, "Ford", "Edge Sport", R.drawable.fordedge, "The SUV that did the Kesel Run in 3 parsecs!"),
            new Vehicle(2, "BMW", "435i", R.drawable.bmw435i, "You're not too sexy for this car!"),
            new Vehicle(3, "Acura", "NSX", R.drawable.acuransx, "Your chance to own a future legend!")

    };
    private final int _id;
    private final String _make;
    private final String _model;
    private final int _imageId;
    private final String _description;

    public Vehicle(
            int id,
            String make,
            String model,
            int imageId,
            String description) {

        _id = id;
        _make = make;
        _model = model;
        _imageId = imageId;
        _description = description;
    }

    public int getId() {
        return _id;
    }

    public String getMake() {
        return _make;
    }

    public String getModel() {
        return _model;
    }

    public int getImageId() {
        return _imageId;
    }

    public String getDescription() {
        return _description;
    }

    @Override
    public String toString() {
        return String.format("%s %s", _make, _model);
    }
}
