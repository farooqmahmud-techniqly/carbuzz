package com.techniqly.carbuzz;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class ShopActivity extends AppCompatActivity {

    public static final String EXTRA_VEHICLE_INDEX = "vehicle_index";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        int vehicleIndex = (int) getIntent().getExtras().get(EXTRA_VEHICLE_INDEX);
        Vehicle vehicle = Vehicle.Vehicles[vehicleIndex];

        ImageView imageView = (ImageView) findViewById(R.id.vehicle_image);
        imageView.setImageResource(vehicle.getImageId());
        imageView.setContentDescription(vehicle.toString());

        TextView vehicleName = (TextView) findViewById(R.id.vehicle_name);
        vehicleName.setText(vehicle.toString());

        TextView vehicleDescription = (TextView) findViewById(R.id.vehicle_description);
        vehicleDescription.setText(vehicle.getDescription());
    }
}
