package com.techniqly.carbuzz;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ShopCategoryActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        ListView listView = getListView();

        ArrayAdapter<Vehicle> vehicleArrayAdapter = new ArrayAdapter<Vehicle>(
                this,
                android.R.layout.simple_list_item_1,
                Vehicle.Vehicles);

        listView.setAdapter(vehicleArrayAdapter);
    }

    @Override
    protected void onListItemClick(ListView listView, View view, int position, long id) {
        Intent intent = new Intent(ShopCategoryActivity.this, ShopActivity.class);
        intent.putExtra(ShopActivity.EXTRA_VEHICLE_INDEX, (int) id);
        startActivity(intent);
    }
}
